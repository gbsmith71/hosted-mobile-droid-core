﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using HostedMobile;
using Xamarin.Forms;
using CarouselView.FormsPlugin.Android;
using Xamarin.Forms.Platform.Android;
using CarouselView.FormsPlugin.Abstractions;

namespace HostedMobile.Droid
{
    [Activity(Label = "HostedMobile.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            CarouselViewRenderer.Init();

            LoadApplication(new App());

			var CustomStatusBarColor = (Color)App.Current.Resources["AndroidStatusBarColor"];
			Window.SetStatusBarColor(CustomStatusBarColor.ToAndroid());
			Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
		}
    }
}
