﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Text;
// using Xamarin.Forms.Platform.Android.AppCompat;
using Xamarin.Forms;
using HostedMobile.Droid;
using HostedMobile;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Android.Graphics;
using Android.Support.V4.View;
using Android.Animation;


[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace HostedMobile.Droid
{
    public class CustomPickerRenderer : PickerRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
		{
			base.OnElementChanged(e);
			var button = e.NewElement;

			if (this.Control != null)
			{
				try
				{
                    var customPicker = e.NewElement as CustomPicker;
                    int id = (int)typeof(Resource.Drawable).GetField(customPicker.IconImage + "_" + customPicker.Theme).GetValue(null);
                    this.Control.Background = this.Resources.GetDrawable(id);

                    var pickerFont = Typeface.CreateFromAsset(Xamarin.Forms.Forms.Context.ApplicationContext.Assets, GlobalVariables.PickerFont + ".otf");
                    Control.Typeface = pickerFont;
					Control.InputType = InputTypes.TextFlagNoSuggestions;
				}
				catch (Exception ex)
				{
				}
			}
		}
	}
}
