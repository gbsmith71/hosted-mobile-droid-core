﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace HostedMobile.Droid
{
	[Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			//Helpers.Settings.PushMessage = Intent.GetStringExtra ("PushMessage") ?? null; 
			base.OnCreate(bundle);
			Thread.Sleep(500); // Simulate a long loading process on app startup.
			StartActivity(typeof(MainActivity));
		}
	}
}